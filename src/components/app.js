import React, { Component } from 'react';
import Archive from "./archive";
import Sidebar from "./sidebar";

export default class App extends Component {
  render() {
    return (
      <div>
        <Sidebar />
        <Archive />
      </div>
    );
  }
}
