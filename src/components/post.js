import React from "react";

const Post = ({date, title, content, author, displayDate}, index) => {
    return (
        <div key={index} className={"archivePost_"+index+" archivePost "+date.substring(0,4)}>
            <p className={"archivePost_"+index+"_title"+" archivePostTitle "}><b>{title}</b></p>
            <p className={"archivePost_"+index+"_author"+" archivePostAuthor"}>By: {author}</p>
            <p className={"archivePost_"+index+"_content"+" archivePostContent"}>{content}</p>
            <p className={"archivePost_"+index+"_date"+" archivePostDate"}><b>{displayDate}</b></p><br />
            
            <div className="archivePostTopLeft" />
            <div className="archivePostTopCenter" />
            <div className="archivePostBottomRight" />
            <div className="archivePostBorder" />
        </div>
    );
};

export default Post;