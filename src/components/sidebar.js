import React, { Component } from "react";

class Sidebar extends Component {
    render() {
        return (
            <div className="sidebar">
                <div className="sidebar_content" id="sidebar">
                    <a href="#" className="dropDown_link" onClick={() => location.reload(true)}><b>Archive</b></a>
                </div>
            </div>
        );
    }
}

export default Sidebar;