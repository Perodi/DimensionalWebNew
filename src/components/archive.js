import React, { Component } from "react";

import Post from "./post";

class Archive extends Component {
    
    constructor() {
        super();
        
        this.postsPreSort = [
            {
                date: "2016-07-30",
                displayDate: "Jul 30, 2016",
                title: "Creation",
                content: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Sem viverra aliquet eget sit amet tellus cras. Ipsum dolor sit amet consectetur adipiscing elit. Nunc pulvinar sapien et ligula ullamcorper malesuada proin libero. Aliquet porttitor lacus luctus accumsan tortor posuere ac ut. Vel pretium lectus quam id leo. Elementum curabitur vitae nunc sed velit dignissim sodales ut. Nulla at volutpat diam ut venenatis tellus in. Orci porta non pulvinar neque laoreet. Volutpat odio facilisis mauris sit amet massa vitae tortor condimentum. Pharetra convallis posuere morbi leo urna. Ornare quam viverra orci sagittis. Amet venenatis urna cursus eget nunc scelerisque viverra mauris. Amet nisl suscipit adipiscing bibendum est ultricies integer quis. Quam id leo in vitae turpis massa sed. Vestibulum morbi blandit cursus risus at ultrices. Sed arcu non odio euismod lacinia at quis risus sed. Integer enim neque volutpat ac tincidunt.",
                author: "Robert Lapray"
            },
            {
                date: "2018-07-29",
                displayDate: "Jul 29, 2018",
                title: "Pre-Creation",
                content: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Sed turpis tincidunt id aliquet risus feugiat in. Bibendum neque egestas congue quisque egestas diam in. In fermentum posuere urna nec tincidunt praesent semper. Amet porttitor eget dolor morbi. Semper feugiat nibh sed pulvinar proin gravida hendrerit. Dui ut ornare lectus sit amet est. Orci a scelerisque purus semper eget duis at. Morbi tempus iaculis urna id volutpat. Diam ut venenatis tellus in metus vulputate. Consectetur purus ut faucibus pulvinar elementum integer enim neque. Elit ut aliquam purus sit. Consectetur purus ut faucibus pulvinar elementum integer. Amet commodo nulla facilisi nullam vehicula ipsum a arcu. Ullamcorper a lacus vestibulum sed arcu. Consequat mauris nunc congue nisi vitae suscipit tellus mauris. Vulputate enim nulla aliquet porttitor. Tristique senectus et netus et malesuada fames ac turpis.",
                author: "Robert Lapray"
            },
            {
                date: "2018-06-30",
                displayDate: "Jun 30, 2018",
                title: "Scrolling",
                content: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. In fermentum et sollicitudin ac orci phasellus egestas tellus rutrum. Imperdiet dui accumsan sit amet nulla facilisi morbi tempus. Lacus laoreet non curabitur gravida arcu. Diam vulputate ut pharetra sit amet aliquam id. Quam pellentesque nec nam aliquam sem. Vehicula ipsum a arcu cursus vitae congue mauris rhoncus. Ante metus dictum at tempor commodo ullamcorper. Velit laoreet id donec ultrices tincidunt arcu non sodales neque. Tristique nulla aliquet enim tortor at. Nisl vel pretium lectus quam id leo in vitae turpis. Ullamcorper velit sed ullamcorper morbi tincidunt ornare. Eget nulla facilisi etiam dignissim diam quis enim lobortis scelerisque. Pretium viverra suspendisse potenti nullam ac.",
                author: "Robert Lapray"
            },
            {
                date: "1989-06-30",
                displayDate: "Jun 30, 1989",
                title: "Archive Bar",
                content: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Proin nibh nisl condimentum id venenatis a condimentum. Et egestas quis ipsum suspendisse ultrices gravida dictum fusce ut. Magna sit amet purus gravida quis blandit. Euismod lacinia at quis risus sed vulputate odio ut enim. Semper feugiat nibh sed pulvinar proin gravida hendrerit lectus a. Id neque aliquam vestibulum morbi blandit cursus risus at. Lectus quam id leo in vitae turpis. Mauris augue neque gravida in fermentum et sollicitudin. Porttitor massa id neque aliquam vestibulum. Sodales ut etiam sit amet nisl purus. Libero volutpat sed cras ornare arcu. Risus at ultrices mi tempus imperdiet nulla malesuada. Dolor morbi non arcu risus. Tristique nulla aliquet enim tortor at auctor. Nibh tortor id aliquet lectus proin nibh nisl. Sagittis id consectetur purus ut faucibus. In massa tempor nec feugiat nisl pretium fusce id. Magna eget est lorem ipsum.",
                author: "Robert Lapray"
            },
            {
                date: "2000-09-05",
                displayDate: "Sep 05, 2000",
                title: "Untitled",
                content: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Nec ullamcorper sit amet risus nullam eget felis. Mi in nulla posuere sollicitudin aliquam ultrices. Euismod elementum nisi quis eleifend quam adipiscing vitae. Accumsan sit amet nulla facilisi morbi tempus iaculis urna. Congue mauris rhoncus aenean vel elit scelerisque mauris pellentesque. Sed adipiscing diam donec adipiscing tristique risus nec. Bibendum ut tristique et egestas quis ipsum. Amet commodo nulla facilisi nullam vehicula ipsum a. Augue eget arcu dictum varius duis at consectetur lorem. Senectus et netus et malesuada fames ac. Neque gravida in fermentum et sollicitudin ac orci phasellus. Suspendisse in est ante in nibh. Volutpat lacus laoreet non curabitur gravida arcu ac tortor.",
                author: "Robert Lapray"
            },
            {
                date: "2014-02-17",
                displayDate: "Feb 02, 2014",
                title: "Finished",
                content: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Elementum curabitur vitae nunc sed velit dignissim sodales ut eu. Rutrum quisque non tellus orci ac auctor augue. Purus sit amet luctus venenatis lectus magna fringilla urna. Gravida arcu ac tortor dignissim convallis aenean et tortor. Pulvinar sapien et ligula ullamcorper malesuada proin. Vulputate dignissim suspendisse in est ante in. Mollis nunc sed id semper risus in hendrerit gravida rutrum. Urna molestie at elementum eu facilisis sed. Eu mi bibendum neque egestas congue. Bibendum at varius vel pharetra vel. Erat velit scelerisque in dictum non. Imperdiet nulla malesuada pellentesque elit eget.",
                author: "Robert Lapray"
            },
            {
                date: "2018-08-06",
                displayDate: "Aug 6, 2018",
                title: "Finally Finished",
                content: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Sed turpis tincidunt id aliquet risus feugiat in. Bibendum neque egestas congue quisque egestas diam in. In fermentum posuere urna nec tincidunt praesent semper. Amet porttitor eget dolor morbi. Semper feugiat nibh sed pulvinar proin gravida hendrerit. Dui ut ornare lectus sit amet est. Orci a scelerisque purus semper eget duis at. Morbi tempus iaculis urna id volutpat. Diam ut venenatis tellus in metus vulputate. Consectetur purus ut faucibus pulvinar elementum integer enim neque. Elit ut aliquam purus sit. Consectetur purus ut faucibus pulvinar elementum integer. Amet commodo nulla facilisi nullam vehicula ipsum a arcu. Ullamcorper a lacus vestibulum sed arcu. Consequat mauris nunc congue nisi vitae suscipit tellus mauris. Vulputate enim nulla aliquet porttitor. Tristique senectus et netus et malesuada fames ac turpis.",
                author: "Robert Lapray"
            },
        ];
    }
    
    componentDidMount() {
        let element = document.createElement("div");
        element.className = "dropDown";
        document.getElementsByClassName("sidebar_content")[0].appendChild(element);
        this.activeArray = [];
        
        for(let i = this.dropDown.length; i > 0; i -= 1) {
            if(this.dropDown[i] !== undefined) {
                document.getElementsByClassName("dropDown")[0].innerHTML += "<a href=\"#\" class=\"dropDown_link\" id=\"dropDown_" + i + "\"><b>" + i + "</b>: " + this.dropDown[i].length + "&nbsp;&nbsp;&nbsp;<i class=\"fas fa-caret-right\"></i></br></a>";
                this.activeArray.push(i);
            }
        }
        
        for(let i = this.activeArray.length - 1; i >= 0; i -= 1) {
            
            let activeArray = this.activeArray
            document.getElementById("dropDown_" + this.activeArray[i]).addEventListener("click", function() {
                
                for(let j = activeArray.length -1; j >= 0; j -=1) {
                    
                    for(let k = document.getElementsByClassName(activeArray[j]).length - 1; k >= 0; k -= 1) {
                        
                        document.getElementsByClassName(activeArray[j])[k].classList.remove("hidden");
                    }
                }
                
                for(let j = activeArray.length - 1; j >= 0; j -= 1) {
                    
                    for(let k = document.getElementsByClassName(activeArray[j]).length - 1; k >= 0; k -= 1) {
                        
                        if(activeArray[i] !== activeArray[j])
                        {
                            document.getElementsByClassName(activeArray[j])[k].classList.add("hidden");
                        }
                    }
                }
                
            });
        }
        
        for(let i = Math.max(...this.activeArray); i >= Math.min(...this.activeArray); i -= 1) {
            
            if(document.getElementsByClassName(i).length !== 0) {
            }
        }
    }
    
    render() {
        
        this.posts = [];
        
        this.dropDown = [];
        
        let currentDate = new Date();
        currentDate = currentDate.toISOString().split('T')[0];
        
        //Sorting function gives all posts a priority number based off of their published date and sorts them in that order.
        for(let i in this.postsPreSort) {
            this.postsPreSort[i].priority = 0x1000 * Math.abs(Number(this.postsPreSort[i].date.substring(0,4)) - Number(currentDate.substring(0,4)));
            this.postsPreSort[i].priority += 0x100 * Math.abs(Number(this.postsPreSort[i].date.substring(5,7)) - Number(currentDate.substring(5,7)));
            this.postsPreSort[i].priority += 0x1 * Math.abs(Number(this.postsPreSort[i].date.substring(8,10)) - Number(currentDate.substring(8,10)));
            this.posts[this.postsPreSort[i].priority] = this.postsPreSort[i];
            this.posts[this.postsPreSort[i].priority].yearMonth = this.postsPreSort[i].date.substring(0,7);
        }
        
        for(let i in this.posts) {
            if(this.posts[i] !== undefined) {
                let date = this.posts[i].yearMonth;
                let year = date.substring(0,4);
                let month = date.substring(5,7);
                
                if(this.dropDown[year] === undefined) {
                    this.dropDown[year] = [];
                }
                if(this.dropDown[year][month] === undefined) {
                    this.dropDown[year][month] = [];
                }
                
                this.dropDown[year][month].push(i);
                this.dropDown[year].length++;
            }
        }
        
        return (
            <div>
                <div className="archivePostContainer">
                    {
                        this.posts.map((data, index) => {
                            return Post(data, index);
                        })
                    }
                </div>
            </div>
        );
    }
}

export default Archive;